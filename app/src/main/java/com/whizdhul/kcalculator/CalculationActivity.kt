package com.whizdhul.kcalculator

import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.os.Bundle
import android.text.Editable
import android.text.InputFilter
import android.text.TextWatcher
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.view.menu.ActionMenuItem
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.whizdhul.kcalculator.classes.EditTextInputDigitFilter
import com.whizdhul.kcalculator.database.entity.Food
import com.whizdhul.kcalculator.database.viewmodel.FoodViewModel

class CalculationActivity : AppCompatActivity() {

    lateinit var foodViewModel: FoodViewModel

    lateinit var food: Food
    lateinit var textViewTitle: TextView
    lateinit var textViewKilocaloriesValue: TextView
    lateinit var textViewKilocalorieValueCalculated: TextView
    lateinit var textViewProteinsValue: TextView
    lateinit var textViewProteinsValueCalculated: TextView
    lateinit var textViewCarbohydratesValue: TextView
    lateinit var textViewCarbohydratesValueCalculated: TextView
    lateinit var textViewFatValue: TextView
    lateinit var textViewFatValueCalculated: TextView
    lateinit var editTextValueCalculated: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_calculation)

        // Get view model provider and set on change action
        foodViewModel = ViewModelProvider(this).get(FoodViewModel::class.java)
        getAllIntent()
        setAllComponent()

        // Set observer on food change (edit and delete)
        foodViewModel.getFoodById(food.id).observe(this, Observer { tmpFood ->
            // Edit food
            if(tmpFood != null){
                println(tmpFood)
                food = tmpFood
                setAllComoponentValues()
                setCalculatedValues()
            } else{ // Delete food
                super.onBackPressed()
            }
        })
    }

    /**
     * Get food selected in previous screen (main activity)
     */
    fun getAllIntent(){
        food = intent.extras?.get("currentFood") as Food
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val menuInflater = MenuInflater(this)
        menuInflater.inflate(R.menu.calculation_menu, menu)
        return true
    }

    /**
     * Manage on click in toolbar
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item is ActionMenuItem){
            super.onBackPressed()
        } else{
            when (item.toString()) {
                resources.getString(R.string.edit_button) -> SharedComponents.updateFoodMainActivity(this, foodViewModel, food)
                resources.getString(R.string.delete_button) -> SharedComponents.deleteFoodMainActivity(this, foodViewModel, food)
                else -> println(item)
            }
        }
        return true
    }

    /**
     * Set all default component when activity start
     */
    fun setAllComponent(){
        // Attibute for all components
        textViewTitle = findViewById<TextView>(R.id.textView_title_food)
        textViewKilocaloriesValue = findViewById<TextView>(R.id.textView_kilocalorie_value)
        textViewKilocalorieValueCalculated = findViewById<TextView>(R.id.textView_kilocalorie_value_calculated)
        textViewProteinsValue = findViewById<TextView>(R.id.textView_proteins_value)
        textViewProteinsValueCalculated = findViewById<TextView>(R.id.textView_proteins_value_calculated)
        textViewCarbohydratesValue = findViewById<TextView>(R.id.textView_carbohydrate_value)
        textViewCarbohydratesValueCalculated = findViewById<TextView>(R.id.textView_carbohydrate_value_calculated)
        textViewFatValue = findViewById<TextView>(R.id.textView_fat_value)
        textViewFatValueCalculated = findViewById<TextView>(R.id.textView_fat_value_calculated)

        editTextValueCalculated = findViewById<EditText>(R.id.editText_value_calculated)
        editTextValueCalculated.filters = arrayOf<InputFilter>(EditTextInputDigitFilter(5, 1))

        // Set on change listener when change grams
        editTextValueCalculated.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                setCalculatedValues()
            }
            override fun afterTextChanged(s: Editable?) {}
        })
    }

    /**
     * Set all values tu change when edit food
     */
    fun setAllComoponentValues(){
        // Set values in strings
        textViewTitle.text = food.name
        textViewKilocaloriesValue.text = food.kilocalories.toString()
        textViewProteinsValue.text = food.proteins.toString()
        textViewCarbohydratesValue.text = food.carbohydrate.toString()
        textViewFatValue.text = food.fat.toString()
    }

    /**
     * Calculate value to display in each row when editText change
     */
    fun setCalculatedValues(){
        var calculationValue = editTextValueCalculated.text.toString().toFloatOrNull()

        if(calculationValue != null){
            var kilocalories = (textViewKilocaloriesValue.text.toString().toFloat() * calculationValue / 100)
            textViewKilocalorieValueCalculated.text = String.format("%.1f", kilocalories)
            var proteins = textViewProteinsValue.text.toString().toFloat() * calculationValue / 100
            textViewProteinsValueCalculated.text = String.format("%.1f", proteins)
            var carbohydrates = textViewCarbohydratesValue.text.toString().toFloat() * calculationValue / 100
            textViewCarbohydratesValueCalculated.text = String.format("%.1f", carbohydrates)
            var fat = textViewFatValue.text.toString().toFloat() * calculationValue / 100
            textViewFatValueCalculated.text = String.format("%.1f", fat)
        } else {
            textViewKilocalorieValueCalculated.text = 0.toString()
            textViewProteinsValueCalculated.text = 0.toString()
            textViewCarbohydratesValueCalculated.text = 0.toString()
            textViewFatValueCalculated.text = 0.toString()
        }
    }

    /**
     * set button back to main screen
     */
    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }

}
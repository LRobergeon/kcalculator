package com.whizdhul.kcalculator

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.res.Resources
import android.text.InputFilter
import android.view.View
import android.view.WindowManager
import com.whizdhul.kcalculator.classes.EditTextInputDigitFilter
import com.whizdhul.kcalculator.database.entity.Food
import com.whizdhul.kcalculator.database.viewmodel.FoodViewModel
import kotlinx.android.synthetic.main.add_food.*
import kotlinx.android.synthetic.main.update_food.*

class SharedComponents {

    companion object{
        fun addFoodMainActivity(context: Context, foodViewModel: FoodViewModel){
            val addFoodDialog = Dialog(context, R.style.Theme_AppCompat_Dialog)
            addFoodDialog.setCancelable(false)
            addFoodDialog.setContentView(R.layout.add_food)

            // Tricks to match windows with content
            setSizePopupWindowsToContent(addFoodDialog)
            // Set size limit for input
            addFoodDialog.editText_add_kilocalories_value.filters = arrayOf<InputFilter>(
                EditTextInputDigitFilter(5, 1))

            addFoodDialog.button_add_food.setOnClickListener{ view ->
                val name = addFoodDialog.editText_add_name_value.text.toString()
                val kilocalories = addFoodDialog.editText_add_kilocalories_value.text.toString().toFloatOrNull()

                if(checkValidFood(name, kilocalories)){
                    val food = Food(0, name, kilocalories, 0f, 0f, 0f)
                    foodViewModel.addFood(food)
                    addFoodDialog.dismiss()
                }
            }

            addFoodDialog.button_cancel_add_food.setOnClickListener{ view ->
                addFoodDialog.dismiss()
            }

            addFoodDialog.show()
        }

        fun updateFoodMainActivity(context: Context, foodViewModel: FoodViewModel, food: Food){
            val updateFoodDialog = Dialog(context, R.style.Theme_AppCompat_Dialog)
            updateFoodDialog.setCancelable(false)
            updateFoodDialog.setContentView(R.layout.update_food)

            // Tricks to match windows with content
            setSizePopupWindowsToContent(updateFoodDialog)
            // Set size limite for input
            updateFoodDialog.editText_update_kiloralories_value.filters = arrayOf<InputFilter>(EditTextInputDigitFilter(5, 1))

            // Set text you want to update in edittext
            updateFoodDialog.editText_update_name_value.setText(food.name)
            updateFoodDialog.editText_update_kiloralories_value.setText(food.kilocalories.toString())
            updateFoodDialog.editText_update_proteins_value.setText(food.proteins.toString())
            updateFoodDialog.editText_update_carbohydrate_value.setText(food.carbohydrate.toString())
            updateFoodDialog.editText_update_fat_value.setText(food.fat.toString())

            updateFoodDialog.button_update_food.setOnClickListener{ view ->
                val updatedName = updateFoodDialog.editText_update_name_value.text.toString()
                val updatedKilocalories = updateFoodDialog.editText_update_kiloralories_value.text.toString().toFloatOrNull()
                val updatedProteins = updateFoodDialog.editText_update_proteins_value.text.toString().toFloatOrNull()
                val updatedCarbohydrate = updateFoodDialog.editText_update_carbohydrate_value.text.toString().toFloatOrNull()
                val updatedFat = updateFoodDialog.editText_update_fat_value.text.toString().toFloatOrNull()

                if(checkValidFood(updatedName, updatedKilocalories)){
                    val updatedFood = Food(food.id, updatedName, updatedKilocalories, updatedProteins, updatedCarbohydrate, updatedFat)
                    println("updatedFood")
                    println(updatedFood)
                    foodViewModel.updateFood(updatedFood)
                    updateFoodDialog.dismiss()
                }
            }

            updateFoodDialog.button_cancel_update_food.setOnClickListener{ view ->
                updateFoodDialog.dismiss()
            }

            updateFoodDialog.show()
        }

        fun deleteFoodMainActivity(context: Context, foodViewModel: FoodViewModel, food: Food){
            val resources: Resources = context.resources
            val deleteFoodBuilder = AlertDialog.Builder(context)
            deleteFoodBuilder.setTitle(resources.getString(R.string.alertDialog_delete_food_title))
            deleteFoodBuilder.setMessage(resources.getString(R.string.alertDialog_delete_food_message) + food.name)
            deleteFoodBuilder.setPositiveButton(resources.getString(R.string.yes)){ dialogInterface, which ->
                foodViewModel.deleteFood(food)
                dialogInterface.dismiss()
            }

            deleteFoodBuilder.setNegativeButton(resources.getString(R.string.no)){ dialogInterface, which ->
                dialogInterface.dismiss()
            }

            val deleteFoodAlertDialog: AlertDialog = deleteFoodBuilder.create()
            deleteFoodAlertDialog.show()
        }

        fun checkValidFood(name: String, kilocalories: Float?): Boolean{
            if(name.isNotEmpty() && kilocalories != null){
                return true
            }
            return false
        }

        fun setSizePopupWindowsToContent(foodDialog: Dialog){
            val lp = WindowManager.LayoutParams()
            lp.copyFrom(foodDialog.window!!.attributes)
            lp.width = WindowManager.LayoutParams.MATCH_PARENT
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT
            foodDialog.window!!.attributes = lp
        }
    }
}
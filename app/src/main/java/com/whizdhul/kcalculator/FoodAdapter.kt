package com.whizdhul.kcalculator

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.res.Resources
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.whizdhul.kcalculator.database.entity.Food
import kotlinx.android.synthetic.main.row_food.view.*


class FoodAdapter(private val context: Context, private val foodList: List<Food>): RecyclerView.Adapter<FoodViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FoodViewHolder {
        return FoodViewHolder(
            LayoutInflater.from(context).inflate(R.layout.row_food, parent, false)
        )
    }

    override fun onBindViewHolder(foodViewHolder: FoodViewHolder, position: Int) {
        val resources: Resources = context.resources

        val food = foodList[position]
        foodViewHolder.textViewFoodRowName.text = food.name

        if(position % 2 == 0){
            foodViewHolder.linearLayoutFoodRow.setBackgroundColor(
                ContextCompat.getColor(context, R.color.linearLayout_row_color_change)
            )
        }

        foodViewHolder.linearLayoutFoodRow.setOnClickListener{ view ->
            println("click")
            if (context is MainActivity){
                val calculationIntent = Intent(context, CalculationActivity::class.java)
                calculationIntent.putExtra("currentFood", food)
                context.startActivity(calculationIntent)
            }
        }

        val choicesOption = arrayOf<CharSequence>(resources.getString(R.string.edit_button)
            , resources.getString(R.string.delete_button))
        foodViewHolder.linearLayoutFoodRow.setOnLongClickListener {
            if (context is MainActivity){
                val builder = AlertDialog.Builder(context)
                builder.setTitle(resources.getString(R.string.dialog_manage_title))
                builder.setItems(
                    choicesOption,
                    DialogInterface.OnClickListener { dialog, item -> // will toast your selection
                        when (choicesOption[item]) {
                            resources.getString(R.string.edit_button) -> context.updateFood(food)
                            resources.getString(R.string.delete_button) -> context.deleteFood(food)
                            else -> println("Nothing")
                        }
                        dialog.dismiss()
                    }
                ).show()
            }
            true // Don't consume event, if return false. Consume event if true.
        }
    }

    override fun getItemCount(): Int {
        return foodList.size
    }
}

class FoodViewHolder(view: View): RecyclerView.ViewHolder(view){
    val linearLayoutFoodRow: LinearLayout = view.linearLayout_food_row
    val textViewFoodRowName: TextView = view.textView_food_row_name
}
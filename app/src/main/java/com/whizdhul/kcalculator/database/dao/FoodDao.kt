package com.whizdhul.kcalculator.database.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.whizdhul.kcalculator.database.entity.Food

@Dao
interface FoodDao {

    @Query("SELECT * FROM food ORDER BY name ASC")
    fun getAllFood(): LiveData<List<Food>>

    @Query("SELECT * FROM food WHERE food.id = :id")
    fun getFoodById(id: Int): LiveData<Food>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun addFood(food: Food)

    @Update(onConflict = OnConflictStrategy.IGNORE)
    suspend fun updateFood(food: Food)

    @Delete
    suspend fun deleteFood(food: Food)
}
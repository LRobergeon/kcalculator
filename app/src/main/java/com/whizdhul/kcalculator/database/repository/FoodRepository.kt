package com.whizdhul.kcalculator.database.repository

import androidx.lifecycle.LiveData
import com.whizdhul.kcalculator.database.entity.Food
import com.whizdhul.kcalculator.database.dao.FoodDao

class FoodRepository(private val foodDao: FoodDao) {

    val getAllFood: LiveData<List<Food>> = foodDao.getAllFood()

    fun getFoodById(id: Int): LiveData<Food>{
        return foodDao.getFoodById(id)
    }

    suspend fun addFood(food: Food){
        foodDao.addFood(food)
    }

    suspend fun updateFood(food: Food){
        foodDao.updateFood(food)
    }

    suspend fun deleteFood(food: Food){
        foodDao.deleteFood(food)
    }
}
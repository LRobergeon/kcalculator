package com.whizdhul.kcalculator.database.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity(tableName = "food")
class Food(
        @PrimaryKey(autoGenerate = true)
        val id: Int,
        var name: String,
        var kilocalories: Float?,
        var proteins: Float?,
        var carbohydrate: Float?,
        var fat: Float?
): Serializable{

    override fun toString(): String {
        return "FOOD " + id +
                " : name = " + name +
                ", kilocalories = " + kilocalories +
                ", proteins = " + proteins +
                ", carbohydrate = " + carbohydrate +
                ", fat = " + fat
    }
}

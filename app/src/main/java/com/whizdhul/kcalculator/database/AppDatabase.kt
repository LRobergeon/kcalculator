package com.whizdhul.kcalculator.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.whizdhul.kcalculator.database.entity.Food
import com.whizdhul.kcalculator.database.dao.FoodDao

@Database(entities = [Food::class], version = 1, exportSchema = false)
abstract class AppDatabase: RoomDatabase() {

    abstract fun foodDao(): FoodDao

    companion object{
        private val DATABASE_NAME = "KCalculator"

        @Volatile
        var INSTANCE: AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase{
            val temporaryInstance = INSTANCE
            if(temporaryInstance != null){
                return temporaryInstance
            }

            synchronized(this){
                val instance = Room.databaseBuilder(
                        context.applicationContext,
                        AppDatabase::class.java,
                        DATABASE_NAME
                ).build()
                INSTANCE = instance
                return instance
            }
        }
    }
}
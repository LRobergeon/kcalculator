package com.whizdhul.kcalculator.database.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.whizdhul.kcalculator.database.entity.Food
import com.whizdhul.kcalculator.database.AppDatabase
import com.whizdhul.kcalculator.database.repository.FoodRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class FoodViewModel(application: Application): AndroidViewModel(application) {

    val getAllFood: LiveData<List<Food>>
    private val foodRepository: FoodRepository

    init {
        val foodDao = AppDatabase.getDatabase(application).foodDao()
        foodRepository = FoodRepository(foodDao)
        getAllFood = foodRepository.getAllFood
    }

    fun getFoodById(id: Int): LiveData<Food>{
        return foodRepository.getFoodById(id)
    }

    fun addFood(food: Food){
        println("2")
        println(food)
        viewModelScope.launch(Dispatchers.IO){
            foodRepository.addFood(food)
        }
    }

    fun updateFood(food: Food){
        viewModelScope.launch(Dispatchers.IO){
            foodRepository.updateFood(food)
        }
    }

    fun deleteFood(food: Food){
        viewModelScope.launch(Dispatchers.IO){
            foodRepository.deleteFood(food)
        }
    }
}
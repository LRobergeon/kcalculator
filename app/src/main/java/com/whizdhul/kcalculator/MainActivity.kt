package com.whizdhul.kcalculator

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.whizdhul.kcalculator.database.entity.Food
import com.whizdhul.kcalculator.database.viewmodel.FoodViewModel
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(){

    private lateinit var foodViewModel: FoodViewModel

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        foodViewModel = ViewModelProvider(this).get(FoodViewModel::class.java)
        foodViewModel.getAllFood.observe(this, Observer { foodList ->
            // Display list if not empty, else display text
            if(foodList.isNotEmpty()){
                println(foodList)
                textView_no_food_list.visibility = View.GONE
                recyclerView_list_food.visibility = View.VISIBLE
                recyclerView_list_food.layoutManager = LinearLayoutManager(this)
                val foodAdapter = FoodAdapter(this, foodList)
                recyclerView_list_food.adapter = foodAdapter
            } else{
                textView_no_food_list.visibility = View.VISIBLE
                recyclerView_list_food.visibility = View.GONE
            }

            val foodAdapter = FoodAdapter(this, foodList)
            recyclerView_list_food.adapter = foodAdapter
        })

        val buttonAddRow = button_add_row
        buttonAddRow.setOnClickListener{ view ->
            addFood()
        }
    }

    fun addFood(){
        SharedComponents.addFoodMainActivity(this, foodViewModel)
    }

    fun updateFood(food: Food){
        SharedComponents.updateFoodMainActivity(this, foodViewModel, food)
    }

    fun deleteFood(food: Food){
        SharedComponents.deleteFoodMainActivity(this, foodViewModel, food)
    }
}
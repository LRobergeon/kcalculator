package com.whizdhul.kcalculator.classes

import android.text.InputFilter
import android.text.Spanned
import java.util.regex.Pattern

class EditTextInputDigitFilter(digitsBeforeZero: Int, digitsAfterZero: Int): InputFilter {

    var mPattern: Pattern? = null

    init{
        mPattern = Pattern.compile("[0-9]{0," + (digitsBeforeZero - 1) + "}+((\\.[0-9]{0," + (digitsAfterZero - 1) + "})?)||(\\.)?")
    }

    override fun filter(source: CharSequence?, start: Int, end: Int, dest: Spanned?, dstart: Int, dend: Int): CharSequence? {
        var matcher = mPattern?.matcher(dest)
        if (matcher != null) {
            if(!matcher.matches())
                return ""
        };
        return null
    }
}